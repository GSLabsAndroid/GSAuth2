# GSAuth2

[![](https://jitpack.io/v/com.gitlab.GSLabsAndroid/GSAuth2.svg)](https://jitpack.io/#com.gitlab.GSLabsAndroid/GSAuth2)

GSLabs SmartHome authorization SDK for Android.

- [Installation](#installation)
- [API](#api)
    - [Database](#database)
    - [Client](#client)
- [Examples](#examples)
    - [Preparation](#preparation)
    - [Login](#login)
    - [Account synchronization](#account-synchronization)
    - [Domain creation](#domain-creation)
    - [Guest invitation](#guest-invitation)
- [Screenshots](#screenshots)
- [TODO](#todo)

## Installation

build.gradle (Project)

```gradle
allprojects {
    repositories {
        maven {
            url "https://jitpack.io"
        }
    }
}
```

build.gradle (Module)

```gradle
dependencies {
    implementation "com.gitlab.GSLabsAndroid:GSAuth2:1.2"
}
```

For compatibility with the newest Java APIs, don't forget to enable [desugaring](https://developer.android.com/studio/write/java8-support#library-desugaring).

## API

The API consists of 2 main interfaces - Database and Client.

### Database

`Gsa2Database` class provides methods to fetch the [entities](images/entities.png) loaded by Client - Accounts, Domains, Controllers, etc. This data can be mapped directly to UI.

### Client

`Gsa2Client` class provides high level API for making backend [requests](https://smart-home-cloud.test.gs-labs.tv/swagger) - GetAccount, CreateDomain, AddControllers, etc. Upon request completion it stores the received [entities](images/entities.png) to Database and invokes asynchronous callbacks, provided by user as lambdas. All callbacks are posted to the application's main thread.

## Examples

You can explore the sample app, provided in `application-gsauth` module. Common use cases are described below.

### Preparation

Client and Database instances should be accessible across application. A good place to store them is `Application` subclass. Custom singleton class also can be used for that.

```java
public class MyApplication extends Application {
    public Gsa2Database database;
    public Gsa2Client client;
}
```

Datasource file, required for Database, is bundled into library's `assets` directory. It can be accessed by `AssetManager`. Howewer, you need to put this file into location with write permissions enabled. Internal storage can be used for that.

```java
File file = new File(getFilesDir(), Gsa2Database.FILE);

if (!file.exists()) {
    try (InputStream inputStream = getAssets().open(Gsa2Database.FILE); FileOutputStream outputStream = openFileOutput(Gsa2Database.FILE, MODE_PRIVATE)) {
        FileUtils.copy(inputStream, outputStream);
    }
}
```

Finally, initialize database and client variables.

```java
database = Gsa2Database.database(file.getAbsolutePath());
client = Gsa2Client.client("https://smart-home-cloud.test.gs-labs.tv", database);
```

That's it! Now you can access the Database and Client instances from whatever `Activity` you want.

### Login

Login means to obtain a Token for given Account, which is used for making future requests.

```java
Gsa2Identity identity = new Gsa2Identity();
identity.type = Gsa2Identity.TYPE_PHONE;
identity.value = "79999999955";

Gsa2Credential credential = new Gsa2Credential();
credential.type = Gsa2Credential.TYPE_PASSWORD;
credential.value = "qwerty1234";

getApplication().client.tokenIssue(identity, credential, (token, exception) -> {
    if (token == null) {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    } else {
        // Success. Use token.access for protected requests.
    }
});
```

At this point Token is cached in Database and can be retrieved for future requests as follows:

```java
Gsa2Token token = getApplication().database.tokenSelectBySubjectAndId(Gsa2Token.SUBJECT_ACCOUNT, accountId)[0];
```

### Account synchronization

This is the main synchronization request, which pulls down Account info, Domains and Controllers, belonging to the given Account.

```java
getApplication().client.accountGet(token.access, (account, exception) -> {
    if (account == null) {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    } else {
        reload();
    }
});
```

`reload()` method can have the following imlementation:

```java
Gsa2DomainFull[] domainsFull = getApplication().database.domainFullSelectByAccount(accountId, "ORDER BY CAST(domain AS INTEGER) DESC");
GsaHomesAdapter adapter = new GsaHomesAdapter(domainsFull);
recyclerView.setAdapter(adapter);
```

### Domain creation

Domain entity represents the home, owned by Account (Apartment, Cottage, etc). You can create one using the code below.

```java
Gsa2Domain domain = new Gsa2Domain();
domain.name = "My Home";

getApplication().client.domainCreate(domain, null, token.access, (domainFull, exception) -> {
    if (domainFull == null) {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    } else {
        // Home created
    }
});
```

Then you can add Controllers and invite Guests to your Home.

Controllers are used to manage appliances, located at your Home.

Guests are other Accounts, which have access to your Home and can manage appliances located there.

### Guest invitation

```java
Gsa2Invitation invitation = new Gsa2Invitation();
invitation.name = "Welcome to My Home";
invitation.expiration = Instant.ofEpochSecond(86400); // Can be accepted in 24 hours
invitation.access = 604800; // Access for 7 days since accepted

getApplication().client.invitationCreate(invitation, domainId, token.access, (invitation, exception) -> {
    if (invitation == null) {
        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    } else {
        // Generate QR code, based on invitation.id and show it to the Guest
    }
});
```

## Screenshots

Illustrate the sample app functionality.

| Login | Account | Homes | Owner | Guest |
| ------ | ------ | ------ | ------ | ------ |
| ![](images/login.jpg) | ![](images/account.jpg) | ![](images/homes.jpg) | ![](images/owner.jpg) | ![](images/guest.jpg) |

## TODO

Compile binaries for x86 ABI (Emulator support).
